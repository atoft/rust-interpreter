use std::collections::HashMap;

use crate::{error::error_for_token, lexer::{Literal, Token, TokenType}};
use crate::expr::*;
use crate::program::*;
use crate::stmt::*;

pub struct Parser {
    tokens: Vec<Token>,
    current: usize,

    program: Program,
}

const MAX_ARGUMENTS_LEN: usize = 255;

impl Parser {
    pub fn parse(tokens: Vec<Token>) -> Result<Program, String> {
        let mut parser = Parser {
            tokens,
            current: 0,
            program: Program {
                main: Vec::new(),
                expressions: Vec::new(),
                statements: Vec::new(),
                resolves: HashMap::new(),
            },
        };

        while !parser.is_at_end() {
            let stmt = parser.declaration()?;
            parser.program.main.push(stmt);
        }

        Ok(parser.program)
    }

    fn declaration(&mut self) -> Result<StmtId, String> {
        let stmt = if self.match_one(TokenType::Class) {
            self.class_declaration()
        } else if self.match_one(TokenType::Fun) {
            self.function("function")
        } else if self.match_one(TokenType::Var) {
            self.var_declaration()
        } else {
            self.statement()
        };

        return match stmt {
            Ok(statement) => Ok(statement),
            Err(e) => {
                self.synchronize();
                Err(e)
            },
        };
    }

    fn class_declaration(&mut self) -> Result<StmtId, String> {
        let name = self.consume(&TokenType::Identifier, String::from("Expect class name."))?;

        let superclass = if self.match_one(TokenType::Less) {
            let subclass_name = self.consume(&TokenType::Identifier, String::from("Expect superclass name"))?;
            Some(self.program.push_expr(Expr::Var(VarExpr {
                name: subclass_name,
            })))
        } else {
            None
        };

        self.consume(&TokenType::LeftBrace, String::from("Expect '{' after class name."))?;

        let mut methods = Vec::new();
        while !self.check(&TokenType::RightBrace) && !self.is_at_end() {
            methods.push(self.function("method")?);
        }
        self.consume(&TokenType::RightBrace, String::from("Expect '}' after class body."))?;

        Ok(self.program.push_stmt(Stmt::Class(ClassStmt {
            name,
            superclass,
            methods,
        })))
    }

    fn function(&mut self, kind: &str) -> Result<StmtId, String> {
        let name = self.consume(&TokenType::Identifier, format!("Expect {} name.", kind))?;

        self.consume(&TokenType::LeftParen, format!("Expect '(' after {} name.", kind))?;

        let mut params = Vec::new();

        if !self.check(&TokenType::RightParen) {
            loop {
                if params.len() >= MAX_ARGUMENTS_LEN {
                    return Err(error_for_token(self.peek(), format!("Can't have more than {} arguments.", MAX_ARGUMENTS_LEN)));
                }

                params.push(self.consume(&TokenType::Identifier, String::from("Expect parameter name."))?);

                if !self.match_one(TokenType::Comma) {
                    break;
                }
            }
        }

        self.consume(&TokenType::RightParen, String::from("Expect ')' after parameters."))?;
        self.consume(&TokenType::LeftBrace, format!("Expect '{{' before '{}' body.", kind))?;

        let body = self.block()?;

        Ok(self.program.push_stmt(Stmt::Function(FunctionStmt {
            name,
            params,
            body,
        })))
    }

    fn var_declaration(&mut self) -> Result<StmtId, String> {
        let name = self.consume(&TokenType::Identifier, String::from("Expect variable name."))?;

        let initializer = if self.match_one(TokenType::Equal) {
            Some(self.expression()?)
        } else {
            None
        };

        self.consume(&TokenType::Semicolon, String::from("Expect ';' after declaration."))?;

        Ok(self.program.push_stmt(Stmt::Var(VarStmt {
            name,
            initializer,
        })))
    }
    
    fn statement(&mut self) -> Result<StmtId, String> {
        if self.match_one(TokenType::For) {
            return self.for_statement();
        }

        if self.match_one(TokenType::If) {
            return self.if_statement();
        }

        if self.match_one(TokenType::Print) {
            return self.print_statement();
        }

        if self.match_one(TokenType::Return) {
            return self.return_statement();
        }

        if self.match_one(TokenType::While) {
            return self.while_statement();
        }

        if self.match_one(TokenType::Break) {
            return self.break_statement();
        }

        if self.match_one(TokenType::LeftBrace) {
            let block = self.block()?;
            return Ok(self.program.push_stmt(Stmt::Block(BlockStmt {
                statements: block,
            })));
        }

        self.expression_statement()
    }

    // @Lox Change from book - for loop body must be block, rather than any statement.
    fn for_statement(&mut self) -> Result<StmtId, String> {
        self.consume(&TokenType::LeftParen, String::from("Expect '(' after for."))?;

        let initializer = if self.match_one(TokenType::Semicolon) {
            None
        } else if self.match_one(TokenType::Var) {
            Some(self.var_declaration()?)
        } else {
            Some(self.expression_statement()?)
        };

        let condition = if !self.check(&TokenType::Semicolon) {
            Some(self.expression()?)
        } else {
            None
        };
        self.consume(&TokenType::Semicolon, String::from("Expect ';' after loop condition."))?;

        let increment = if !self.check(&TokenType::RightParen) {
            Some(self.expression()?)
        } else {
            None
        };
        self.consume(&TokenType::RightParen, String::from("Expect ')' after 'for' clauses."))?;
        self.consume(&TokenType::LeftBrace, String::from("Expect '{' before 'for' body."))?;

        let block = self.block()?;
        let mut body = self.program.push_stmt(Stmt::Block(BlockStmt {
            statements: block,
        }));

        // Desugar into while loop.

        if let Some(inc) = increment {
            let expr_stmt = self.program.push_stmt(Stmt::Expr(ExpressionStmt {
                expr: inc
            }));

            body = self.program.push_stmt(Stmt::Block(BlockStmt {
                statements: vec![body, expr_stmt],
            }));
        }

        let cond = if let Some(con) = condition {
            con
        } else {
            self.program.push_expr(Expr::Literal(LiteralExpr::Bool(true)))
        };

        body = self.program.push_stmt(Stmt::While(WhileStmt {
            condition: cond,
            body: body
        }));

        if let Some(init) = initializer {
            body = self.program.push_stmt(Stmt::Block(BlockStmt {
                statements: vec![init, body],
            }))
        }

        Ok(body)
    }

    // @Lox Change from book - branches must be blocks, rather than any statement.
    fn if_statement(&mut self) -> Result<StmtId, String> {
        self.consume(&TokenType::LeftParen, String::from("Expect '(' after if."))?;

        let condition = self.expression()?;

        self.consume(&TokenType::RightParen, String::from("Expect ')' after condition."))?;

        self.consume(&TokenType::LeftBrace, String::from("Expect '{' before 'then' branch body."))?;
        let block = self.block()?;
        let then_branch = self.program.push_stmt(Stmt::Block(BlockStmt {
            statements: block
        }));

        let else_branch = if self.match_one(TokenType::Else) {
            self.consume(&TokenType::LeftBrace, String::from("Expect '{' before 'else' branch body."))?;
            let block = self.block()?;
            Some(self.program.push_stmt(Stmt::Block(BlockStmt {
                statements: block
            })))
        } else {
            None
        };

        Ok(self.program.push_stmt(Stmt::If(IfStmt {
            condition,
            then_branch: then_branch,
            else_branch,
        })))
    }

    fn print_statement(&mut self) -> Result<StmtId, String> {
        let value = self.expression()?;

        self.consume(&TokenType::Semicolon, String::from("Expect ';' after value."))?;

        Ok(self.program.push_stmt(Stmt::Print(PrintStmt {
            expr: value,
        })))
    }

    fn return_statement(&mut self) -> Result<StmtId, String> {
        let keyword = self.previous().clone();

        let value = if !self.check(&TokenType::Semicolon) {
            Some(self.expression()?)
        } else {
            None
        };

        self.consume(&TokenType::Semicolon, String::from("Expect ';' after return value."))?;

        Ok(self.program.push_stmt(Stmt::Return(ReturnStmt {
            keyword: keyword.clone(),
            value,
        })))
    }

    // @Lox Change from book - while loop body must be block, rather than any statement.
    fn while_statement(&mut self) -> Result<StmtId, String> {
        self.consume(&TokenType::LeftParen, String::from("Expect '(' after 'while'."))?;

        let condition = self.expression()?;

        self.consume(&TokenType::RightParen, String::from("Expect ')' after condition."))?;
        self.consume(&TokenType::LeftBrace, String::from("Expect '{' before 'while' body."))?;

        let block = self.block()?;
        let body = self.program.push_stmt(Stmt::Block(BlockStmt {
            statements: block,
        }));

        Ok(self.program.push_stmt(Stmt::While(WhileStmt {
            condition,
            body,
        })))
    }

    fn break_statement(&mut self) ->Result<StmtId, String> {
        let location = self.previous().clone();
        self.consume(&TokenType::Semicolon, String::from("Expect ';' after 'break'."))?;

        Ok(self.program.push_stmt(Stmt::Break(BreakStmt {
            location
        })))
    }

    fn expression_statement(&mut self) -> Result<StmtId, String> {
        let value = self.expression()?;

        self.consume(&TokenType::Semicolon, String::from("Expect ';' after expression."))?;

        Ok(self.program.push_stmt(Stmt::Expr(ExpressionStmt {
            expr: value,
        })))
    }

    fn block(&mut self) -> Result<Vec<StmtId>, String> {
        let mut statements = Vec::new();

        while !self.check(&TokenType::RightBrace) && !self.is_at_end() {
            statements.push(self.declaration()?);
        }

        self.consume(&TokenType::RightBrace, String::from("Expect '}' after block."))?;

        Ok(statements)
    }

    fn expression(&mut self) -> Result<ExprId, String> {
        self.assignment()
    }

    fn assignment(&mut self) -> Result<ExprId, String> {
        let expr = self.or()?;

        // NOTE: In this case we undo what was added on the left hand-side if we find an equal sign.
        // This means we have a subset of nodes in the Expr list that are no longer referenced, and
        // we want to remove them. It works because all the added sub-Exprs will be after this in the
        // list. It also assumes that no Stmt can be added beneath an Expr.
        if self.match_one(TokenType::Equal) {
            let equals = self.previous().clone();

            let expr_to_replace = self.program.get_expr(&expr).clone();
            self.program.remove_expr_and_children(expr);
            let value = self.assignment()?;

            let replacement_expr = match expr_to_replace {
                Expr::Var(v) => {
                    let name = v.name.clone();
                    Expr::Assign(AssignExpr {
                        name,
                        value,
                    })
                },
                Expr::Get(v) => {
                    let name = v.name.clone();
                    Expr::Set(SetExpr {
                        object: v.object,
                        name,
                        value,
                    })
                },
                _ => {
                    return Err(error_for_token(&equals, String::from("Invalid assignment target.")))
                },
            };

            return Ok(self.program.push_expr(replacement_expr));
        }

        Ok(expr)
    }

    fn or(&mut self) -> Result<ExprId, String> {
        let mut expr = self.and()?;

        while self.match_one(TokenType::Or) {
            let operator = self.previous().clone();
            let right = self.and()?;
            expr = self.program.push_expr(Expr::Logical(LogicalExpr {
                left: expr,
                operator,
                right,
            }));
        }

        Ok(expr)
    }

    fn and(&mut self) -> Result<ExprId, String> {
        let mut expr = self.equality()?;

        while self.match_one(TokenType::And) {
            let operator = self.previous().clone();
            let right = self.equality()?;
            expr = self.program.push_expr(Expr::Logical(LogicalExpr {
                left: expr,
                operator,
                right,
            }));
        }

        Ok(expr)
    }

    fn equality(&mut self) -> Result<ExprId, String> {
        let mut expr = self.comparison()?;

        while self.match_any_of(&[TokenType::BangEqual, TokenType::EqualEqual]) {
            let operator = self.previous().clone();
            let right = self.comparison()?;
            expr = self.program.push_expr(Expr::Binary(BinaryExpr {
                left: expr,
                operator,
                right,
            }));
        }

        Ok(expr)
    }

    fn comparison(&mut self) -> Result<ExprId, String> {
        let mut expr = self.term()?;

        while self.match_any_of(&[TokenType::Greater, TokenType::GreaterEqual, TokenType::Less, TokenType::LessEqual]) {
            let operator = self.previous().clone();
            let right = self.term()?;
            expr = self.program.push_expr(Expr::Binary(BinaryExpr {
                left: expr,
                operator,
                right,
            }));
        }

        Ok(expr)
    }

    fn term(&mut self) -> Result<ExprId, String> {
        let mut expr = self.factor()?;

        while self.match_any_of(&[TokenType::Minus, TokenType::Plus]) {
            let operator = self.previous().clone();
            let right = self.factor()?;
            expr = self.program.push_expr(Expr::Binary(BinaryExpr {
                left: expr,
                operator,
                right,
            }));
        }

        Ok(expr)
    }

    fn factor(&mut self) -> Result<ExprId, String> {
        let mut expr = self.unary()?;

        while self.match_any_of(&[TokenType::Slash, TokenType::Star]) {
            let operator = self.previous().clone();
            let right = self.unary()?;
            expr = self.program.push_expr(Expr::Binary(BinaryExpr {
                left: expr,
                operator,
                right,
            }));
        }

        Ok(expr)
    }

    fn unary(&mut self) -> Result<ExprId, String> {
        if self.match_any_of(&[TokenType::Bang, TokenType::Minus]) {
            let operator = self.previous().clone();
            let right = self.unary()?;
            return Ok(self.program.push_expr(Expr::Unary(UnaryExpr {
                operator,
                right,
            })));
        }

        self.call()
    }

    fn call(&mut self) -> Result<ExprId, String> {
        let mut expr = self.primary()?;

        loop {
            if self.match_one(TokenType::LeftParen) {
                expr = self.finish_call(expr)?;
            } else if self.match_one(TokenType::Dot) {
                let name = self.consume(&TokenType::Identifier, String::from("Expect property name after '.'."))?;
                expr = self.program.push_expr(Expr::Get(GetExpr {
                    object: expr,
                    name,
                }));
            }
            else {
                break;
            }
        }

        Ok(expr)
    }

    fn finish_call(&mut self, callee: ExprId) -> Result<ExprId, String> {
        let mut arguments = Vec::new();

        if !self.check(&TokenType::RightParen) {
            loop {
                if arguments.len() >= MAX_ARGUMENTS_LEN {
                    error_for_token(self.peek(), format!("Can't have more than {} arguments.", MAX_ARGUMENTS_LEN));
                }
                arguments.push(self.expression()?);
                if !self.match_one(TokenType::Comma) {
                    break;
                }
            }
        }

        let paren = self.consume(&TokenType::RightParen, String::from("Expect ')' after arguments."))?;

        return Ok(self.program.push_expr(Expr::Call(CallExpr {
            callee: callee,
            paren,
            arguments,
        })))
    }

    fn primary(&mut self) -> Result<ExprId, String> {
        if self.match_one(TokenType::False) {
            return Ok(self.program.push_expr(Expr::Literal(LiteralExpr::Bool(false))));
        }
        if self.match_one(TokenType::True) {
            return Ok(self.program.push_expr(Expr::Literal(LiteralExpr::Bool(true))));
        }
        if self.match_one(TokenType::Nil) {
            return Ok(self.program.push_expr(Expr::Literal(LiteralExpr::Nil)));
        }

        if let Some(x) = self.match_number() {
            return Ok(self.program.push_expr(Expr::Literal(LiteralExpr::Number(x))));
        }

        if let Some(x) = self.match_string() {
            return Ok(self.program.push_expr(Expr::Literal(LiteralExpr::String(x))));
        }

        if self.match_one(TokenType::Super) {
            let keyword = self.previous().clone();
            self.consume(&TokenType::Dot, String::from("Expect '.' after 'super'."))?;
            let method = self.consume(&TokenType::Identifier, String::from("Expect superclass method name."))?;
            return Ok(self.program.push_expr(Expr::Super(SuperExpr {
                keyword,
                method,
            })));
        }

        if self.match_one(TokenType::This) {
            return Ok(self.program.push_expr(Expr::This(ThisExpr {
                keyword: self.previous().clone(),
            })));
        }

        if let Some(x) = self.match_identifier() {
            return Ok(self.program.push_expr(Expr::Var(VarExpr {
                name: x,
            })));
        }

        if self.match_one(TokenType::LeftParen) {
            let expr = self.expression()?;

            self.consume(&TokenType::RightParen, String::from("Expect ')' after expression."))?;

            return Ok(self.program.push_expr(Expr::Grouping(GroupingExpr{
                expr})));
        }

        return Err(error_for_token(self.peek(), String::from("Expect expression.")))
    }

    fn match_one(&mut self, token_type: TokenType) -> bool {
        if self.check(&token_type) {
            self.advance();
            return true;
        }
        false
    }

    fn match_any_of(&mut self, types: &[TokenType]) -> bool {
        for token_type in types {
            if self.check(token_type) {
                self.advance();
                return true;
            }
        }

        false
    }

    fn match_number(&mut self) -> Option<f32> {
        if self.is_at_end() {
            return None;
        }

        let token = self.peek();

        if token.token_type == TokenType::Number {
            if let Some(Literal::Number(x)) = token.literal {
                self.advance();
                return Some(x);
            } else {
                // @Lang Pulling literal payloads out of TokenType caused a runtime error where
                // previously a compile error was possible. Could a different tagged union implementation avoid
                // such issues?
                panic!("Missing literal for Number token.");
            }

        }

        None
    }

    fn match_string(&mut self) -> Option<String> {
        if self.is_at_end() {
            return None;
        }

        // @Lang Have to clone the whole token here because of borrowing rules (self.advance() needs mutable borrow).
        // It's cloned even if we don't enter the branch below, where it would be a problem.
        // Ignoring threading, it's protecting from a dangling ref if self.advance() mutated the list of tokens (which cannot actually
        // happen in the concrete example of this program).
        let token = self.peek().clone();

        if token.token_type == TokenType::String {
            if let Some(Literal::String(x)) = token.literal {
                self.advance();
                return Some(x);
            } else {
                panic!("Missing literal for String token.");
            }
        }

        None
    }

    fn match_identifier(&mut self) -> Option<Token> {
        if self.is_at_end() {
            return None;
        }

        let token = self.peek().clone();

        if token.token_type == TokenType::Identifier {
            if let Some(Literal::Identifier(_)) = token.literal {
                self.advance();
                return Some(token);
            } else {
                panic!("Missing literal for Identifier token.");
            }
        }

        None
    }

    fn check(&self, token_type: &TokenType) -> bool {
        if self.is_at_end() {
            return false;
        }

        &self.peek().token_type == token_type
    }

    fn advance(&mut self) -> &Token {
        if !self.is_at_end() {
            self.current += 1;
        }

        self.previous()
    }

    fn is_at_end(&self) -> bool {
        self.peek().token_type == TokenType::EOF
    }

    fn peek(&self) -> &Token {
        &self.tokens[self.current]
    }

    fn previous(&self) -> &Token {
        &self.tokens[self.current - 1]
    }

    fn consume(&mut self, token_type: &TokenType, message: String) -> Result<Token, String> {
        if self.check(token_type) {
            let token = self.advance();
            return Ok(token.clone());
        }

        Err(error_for_token(self.peek(), message))
    }

    fn synchronize(&mut self) {
        self.advance();

        while !self.is_at_end() {
            if self.previous().token_type == TokenType::Semicolon {
                return;
            }

            match self.peek().token_type {
                TokenType::Class | TokenType::Fun | TokenType::Var | TokenType::For | TokenType::If | TokenType::While | TokenType::Print | TokenType::Return => return,
                _ => {
                    self.advance();
                },
            }
        }
    }
}
