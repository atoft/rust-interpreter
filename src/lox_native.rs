use crate::interpreter::{Interpreter, MaybeValue, Value};
use crate::program::Program;

pub struct LoxNative {
    function: Box<dyn Fn(Vec<Value>) -> MaybeValue>,
}

impl LoxNative {
    pub fn new<F>(function: F) -> LoxNative
        where F: Fn(Vec<Value>) -> MaybeValue + 'static {
        LoxNative {
            function: Box::new(function),
        }
    }

    pub fn call(&self, _interpreter: &mut Interpreter, _program: &Program, arguments: Vec<Value>) -> MaybeValue {
        (self.function)(arguments)
    }

    pub fn arity(&self) -> usize {
        0
    }

    pub fn to_string(&self) -> String {
        String::from("clock")
    }
}