use crate::lexer::Token;
use crate::program::*;

#[derive(Debug)]
pub struct ExpressionStmt {
    pub expr: ExprId,
}

#[derive(Debug)]
pub struct FunctionStmt {
    pub name: Token,
    pub params: Vec<Token>,
    pub body: Vec<StmtId>,
}

#[derive(Debug)]
pub struct IfStmt {
    pub condition: ExprId,
    pub then_branch: StmtId,
    pub else_branch: Option<StmtId>,
}

#[derive(Debug)]
pub struct PrintStmt {
    pub expr: ExprId,
}

#[derive(Debug)]
pub struct ReturnStmt {
    pub keyword: Token,
    pub value: Option<ExprId>,
}

#[derive(Debug)]
pub struct VarStmt {
    pub name: Token,
    pub initializer: Option<ExprId>,
}

#[derive(Debug)]
pub struct WhileStmt {
    pub condition: ExprId,
    pub body: StmtId,
}

#[derive(Debug)]
pub struct BreakStmt {
    pub location: Token,
}

#[derive(Debug)]
pub struct BlockStmt {
    pub statements: Vec<StmtId>,
}

#[derive(Debug)]
pub struct ClassStmt {
    pub name: Token,
    pub superclass: Option<ExprId>, // VarExpr
    pub methods: Vec<StmtId>,
}

#[derive(Debug)]
pub enum Stmt {
    Expr(ExpressionStmt),
    Function(FunctionStmt),
    If(IfStmt),
    Print(PrintStmt),
    Return(ReturnStmt),
    Var(VarStmt),
    While(WhileStmt),
    Break(BreakStmt),
    Block(BlockStmt),
    Class(ClassStmt),
}
