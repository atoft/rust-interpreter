use std::collections::HashMap;

use crate::error::error_for_token;
use crate::expr::{AssignExpr, BinaryExpr, CallExpr, Expr, GetExpr, GroupingExpr, LiteralExpr, LogicalExpr, SetExpr, ThisExpr, UnaryExpr, VarExpr, SuperExpr};
use crate::lexer::Token;
use crate::program::{ExprId, Program, StmtId};
use crate::stmt::{BlockStmt, BreakStmt, ClassStmt, ExpressionStmt, FunctionStmt, IfStmt, PrintStmt, ReturnStmt, Stmt, VarStmt, WhileStmt};

#[derive(Copy, Clone,)]
enum FunctionType {
    None,
    Function,
    Initializer,
    Method,
}

#[derive(Copy, Clone)]
enum ClassType {
    None,
    Class,
    Subclass,
}

#[derive(Copy, Clone)]
enum LoopType {
    None,
    Loop,
}

struct Resolution {
    token: Option<Token>,
    assigned: bool,
    referenced: bool,
}

pub struct Resolver {
    resolves: HashMap<ExprId, usize>,
    scopes: Vec<HashMap<String, Resolution>>,
    current_function: FunctionType,
    current_class: ClassType,
    current_loop: LoopType,
}

impl Resolver {
    pub fn resolve(program: &mut Program) -> Result<(), String> {
        let mut resolver = Resolver {
            resolves: HashMap::new(),
            scopes: Vec::new(),
            current_function: FunctionType::None,
            current_class: ClassType::None,
            current_loop: LoopType::None,
        };

        resolver.resolve_program(program)?;

        assert!(resolver.scopes.is_empty(), "Mismatched begin and end scopes!");

        program.resolves = resolver.resolves;

        Ok(())
    }

    fn resolve_program(&mut self, program: &Program) -> Result<(), String> {
        self.resolve_statements(program, &program.main)
    }

    fn resolve_statements(&mut self, program: &Program, statements: &Vec<StmtId>) -> Result<(), String> {
        for stmt in statements {
            self.resolve_statement(program, &stmt)?
        }
        Ok(())
    }

    fn resolve_statement(&mut self, program: &Program, id: &StmtId) -> Result<(), String> {
        let stmt = program.get_stmt(id);
        match stmt {
            Stmt::Expr(s) => self.resolve_expression_stmt(program, &s),
            Stmt::Function(s) => self.resolve_function_stmt(program, &s),
            Stmt::If(s) => self.resolve_if_stmt(program, &s),
            Stmt::Print(s) => self.resolve_print_stmt(program, &s),
            Stmt::Return(s) => self.resolve_return_stmt(program, &s),
            Stmt::Var(s) => self.resolve_var_stmt(program, &s),
            Stmt::While(s) => self.resolve_while_stmt(program, &s),
            Stmt::Break(s) => self.resolve_break_stmt(program, &s),
            Stmt::Block(s) => self.resolve_block_stmt(program, &s),
            Stmt::Class(s) => self.resolve_class_stmt(program, &s),
        }
    }

    fn resolve_expression(&mut self, program: &Program, id: &ExprId) -> Result<(), String> {
        let expr = program.get_expr(id);
        match expr {
            Expr::Assign(e) => self.resolve_assign_expr(program, &id, &e),
            Expr::Binary(e) => self.resolve_binary_expr(program, &e),
            Expr::Call(e) => self.resolve_call_expr(program, &e),
            Expr::Grouping(e) => self.resolve_grouping_expr(program, &e),
            Expr::Literal(e) => self.resolve_literal_expr(program, &e),
            Expr::Unary(e) => self.resolve_unary_expr(program, &e),
            Expr::Logical(e) => self.resolve_logical_expr(program, &e),
            Expr::Var(e) => self.resolve_variable_expr(program, &id, &e),
            Expr::Get(e) => self.resolve_get_expr(program, &e),
            Expr::Set(e) => self.resolve_set_expr(program, &e),
            Expr::Super(e) => self.resolve_super_expr(program, &id, &e),
            Expr::This(e) => self.resolve_this_expr(program, &id, &e),
        }
    }

    fn begin_scope(&mut self)  -> Result<(), String> {
        self.scopes.push(HashMap::new());
        Ok(())
    }

    fn end_scope(&mut self)  -> Result<(), String>{
        let leaving_scope = self.scopes.pop().expect("Mismatched begin and end scopes in Resolver!");

        // NOTE: Only handles local names, catches both unused functions and variables.
        for pair in leaving_scope {
            if !pair.1.referenced {
                if let Some(token) = &pair.1.token {
                    return Err(error_for_token(token, format!("Local '{}' is never used.", &pair.0)));
                }
            }
        }
        Ok(())
    }

    fn resolve_block_stmt(&mut self, program: &Program, stmt: &BlockStmt) -> Result<(), String> {
        self.begin_scope()?;
        self.resolve_statements(program, &stmt.statements)?;
        self.end_scope()?;
        Ok(())
    }

    fn resolve_class_stmt(&mut self, program: &Program, stmt: &ClassStmt) -> Result<(), String> {
        let enclosing_class = self.current_class;
        self.current_class = ClassType::Class;

        self.declare(&stmt.name)?;
        self.define(&stmt.name)?;

        if let Some(superclass) = stmt.superclass {
            self.current_class = ClassType::Subclass;
            if let Expr::Var(superclass_var) = program.get_expr(&superclass) {
                if superclass_var.name.lexeme == stmt.name.lexeme {
                    return Err(error_for_token(&superclass_var.name, String::from("A class can't inherit from itself.")));
                }
            } else {
                panic!("Superclass not a VarExpr!")
            }

            self.resolve_expression(program, &superclass)?;

            self.begin_scope()?;
            self.scopes.last_mut().expect("No scope?").insert(String::from("super"), Resolution {
                token: None,
                assigned: true,
                referenced: true,
            });
        }

        self.begin_scope()?;
        self.scopes.last_mut().expect("No scope?").insert(String::from("this"), Resolution {
            token: None,
            assigned: true,
            referenced: true,
        });

        for method in &stmt.methods {
            let method_stmt = program.get_stmt(&method);
            if let Stmt::Function(func) = method_stmt {
                let function_type = if func.name.lexeme == "init" {
                    FunctionType::Initializer
                } else {
                    FunctionType::Method
                };
                self.resolve_function(program, &func, function_type)?;
            } else {
                panic!("Method contained a Stmt that was not a function!");
            }
        }

        self.end_scope()?;

        if let Some(_) = stmt.superclass {
            self.end_scope()?;
        }

        self.current_class = enclosing_class;
        Ok(())
    }

    fn resolve_expression_stmt(&mut self, program: &Program, stmt: &ExpressionStmt) -> Result<(), String> {
        self.resolve_expression(program, &stmt.expr)
    }

    fn resolve_function_stmt(&mut self, program: &Program, stmt: &FunctionStmt) -> Result<(), String> {
        self.declare(&stmt.name)?;
        self.define(&stmt.name)?;
        self.resolve_function(program, stmt, FunctionType::Function)
    }

    fn resolve_if_stmt(&mut self, program: &Program, stmt: &IfStmt) -> Result<(), String> {
        self.resolve_expression(program, &stmt.condition)?;
        self.resolve_statement(program, &stmt.then_branch)?;

        if let Some(branch) = &stmt.else_branch {
            self.resolve_statement(program, branch)?;
        }

        Ok(())
    }

    fn resolve_print_stmt(&mut self, program: &Program, stmt: &PrintStmt) -> Result<(), String> {
        self.resolve_expression(program, &stmt.expr)
    }

    fn resolve_return_stmt(&mut self, program: &Program, stmt: &ReturnStmt) -> Result<(), String> {
        if let FunctionType::None = self.current_function {
            return Err(error_for_token(&stmt.keyword, String::from("Can't return from top level code.")));
        }

        if let Some(value) = &stmt.value {
            if let FunctionType::Initializer = self.current_function {
                return Err(error_for_token(&stmt.keyword, String::from("Can't return a value from an initializer.")));
            }
            self.resolve_expression(program, value)?;
        }
        Ok(())
    }

    fn resolve_while_stmt(&mut self, program: &Program, stmt: &WhileStmt) -> Result<(), String> {
        self.resolve_expression(program, &stmt.condition)?;

        let enclosing_loop = self.current_loop;
        self.current_loop = LoopType::Loop;

        self.resolve_statement(program, &stmt.body)?;

        self.current_loop = enclosing_loop;
        Ok(())
    }

    fn resolve_break_stmt(&mut self, _program: &Program, stmt: &BreakStmt) -> Result<(), String> {
        if let LoopType::None = self.current_loop {
            return Err(error_for_token(&stmt.location, String::from("Can't break not inside loop.")));
        }
        Ok(())
    }

    fn resolve_function(&mut self, program: &Program, stmt: &FunctionStmt, function_type: FunctionType) -> Result<(), String> {
        let enclosing_function = self.current_function;
        self.current_function = function_type;

        self.begin_scope()?;

        for param in &stmt.params {
            self.declare(param)?;
            self.define(param)?;
        }
        self.resolve_statements(program, &stmt.body)?;
        self.end_scope()?;

        self.current_function = enclosing_function;

        Ok(())
    }

    fn resolve_var_stmt(&mut self, program: &Program, stmt: &VarStmt) -> Result<(), String> {
        self.declare(&stmt.name)?;
        if let Some(init) = stmt.initializer {
            self.resolve_expression(program, &init)?;
        }
        self.define(&stmt.name)
    }

    fn resolve_assign_expr(&mut self, program: &Program, id: &ExprId, expr: &AssignExpr) -> Result<(), String> {
        self.resolve_expression(program, &expr.value)?;
        self.resolve_local(id, &expr.name)
    }

    fn resolve_binary_expr(&mut self, program: &Program, expr: &BinaryExpr) -> Result<(), String> {
        self.resolve_expression(program, &expr.left)?;
        self.resolve_expression(program, &expr.right)
    }

    fn resolve_call_expr(&mut self, program: &Program, expr: &CallExpr) -> Result<(), String> {
        self.resolve_expression(program, &expr.callee)?;

        for arg in &expr.arguments {
            self.resolve_expression(program, arg)?;
        }

        Ok(())
    }

    fn resolve_grouping_expr(&mut self, program: &Program, expr: &GroupingExpr) -> Result<(), String> {
        self.resolve_expression(program, &expr.expr)
    }

    fn resolve_literal_expr(&mut self, _program: &Program, _expr: &LiteralExpr) -> Result<(), String> {
        Ok(())
    }

    fn resolve_logical_expr(&mut self, program: &Program, expr: &LogicalExpr) -> Result<(), String> {
        self.resolve_expression(program, &expr.left)?;
        self.resolve_expression(program, &expr.right)
    }

    fn resolve_unary_expr(&mut self, program: &Program, expr: &UnaryExpr) -> Result<(), String> {
        self.resolve_expression(program, &expr.right)
    }

    fn resolve_variable_expr(&mut self, _program: &Program, var_expr_id: &ExprId, var_expr: &VarExpr) -> Result<(), String> {
        if let Some(scope) = self.scopes.last() {
            if let Some(resolution) = scope.get(&var_expr.name.lexeme) {
                if !resolution.assigned {
                    return Err(error_for_token(&var_expr.name, String::from("Can't read local variable in its own initializer.")));
                }
            }
        }

        self.resolve_local(&var_expr_id, &var_expr.name)
    }

    fn resolve_get_expr(&mut self, program: &Program, get_expr: &GetExpr) -> Result<(), String> {
        self.resolve_expression(program, &get_expr.object)
    }

    fn resolve_set_expr(&mut self, program: &Program, set_expr: &SetExpr) -> Result<(), String> {
        self.resolve_expression(program, &set_expr.value)?;
        self.resolve_expression(program, &set_expr.object)
    }

    fn resolve_super_expr(&mut self, _program: &Program, id: &ExprId, super_expr: &SuperExpr) -> Result<(), String> {
        match self.current_class {
            ClassType::Subclass => self.resolve_local(id, &super_expr.keyword),
            ClassType::Class => Err(error_for_token(&super_expr.keyword, String::from("Can't use 'super' in a class with no superclass."))),
            ClassType::None => Err(error_for_token(&super_expr.keyword, String::from("Can't use 'super' outside of a class."))),
        }
    }

    fn resolve_this_expr(&mut self, _program: &Program, id: &ExprId, this_expr: &ThisExpr) -> Result<(), String> {
        if let ClassType::None = self.current_class {
            return Err(error_for_token(&this_expr.keyword, String::from("Can't use 'this' outside of a class.")));
        }

        self.resolve_local(id, &this_expr.keyword)
    }

    fn declare(&mut self, name: &Token) -> Result<(), String> {
        if let Some(scope) = self.scopes.last_mut() {
            if scope.contains_key(&name.lexeme) {
                return Err(error_for_token(name, String::from("Already a variable with this name in this scope.")));
            }
            scope.insert(name.lexeme.clone(), Resolution { token: Some(name.clone()), assigned: false, referenced: false, });
        }
        Ok(())
    }

    fn define(&mut self, name: &Token) -> Result<(), String> {
        if let Some(scope) = &mut self.scopes.last_mut() {
            scope.insert(name.lexeme.clone(), Resolution { token: Some(name.clone()), assigned: true, referenced: false, });
        }
        Ok(())
    }

    fn resolve_local(&mut self, id: &ExprId, name: &Token) -> Result<(), String> {
        let scopes_len = self.scopes.len();
        for (idx, scope) in self.scopes.iter_mut().enumerate().rev() {
            if let Some(resolution) = scope.get_mut(&name.lexeme) {
                resolution.referenced = true;
                self.resolves.insert(id.clone(), scopes_len - 1 - idx);
            }
        }
        Ok(())
    }
}