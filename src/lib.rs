use lexer::Lexer;
use parser::Parser;

use crate::interpreter::Interpreter;
use crate::resolver::Resolver;

pub mod environment;
pub mod error;
pub mod expr;
pub mod interpreter;
pub mod lexer;
pub mod parser;
pub mod program;
pub mod resolver;
pub mod lox_native;
pub mod stmt;
mod lox_function;
mod lox_class;
mod lox_instance;

#[derive(Debug)]
pub enum RunFileError {
    IOError,
    LexerError,
    ParseError,
    ResolutionError,
    RuntimeError,
}

pub fn run_file(path: String) -> Result<(), RunFileError> {
    println!("Passed path {}", path);

    let contents = match std::fs::read_to_string(path) {
        Ok(c) => c,
        Err(_) => return Err(RunFileError::IOError),
    };

    let tokens = match Lexer::scan_tokens(&contents) {
        Ok(t) => t,
        Err(_) => return Err(RunFileError::LexerError),
    };

    let mut program = match Parser::parse(tokens) {
        Ok(expr) => expr,
        Err(_) => return Err(RunFileError::ParseError),
    };

    match Resolver::resolve(&mut program) {
        Ok(()) => (),
        Err(_) => return Err(RunFileError::ResolutionError),
    }

    let success = Interpreter::interpret(program);

    if !success {
        return Err(RunFileError::RuntimeError);
    }

    return Ok(());
}

pub fn run_prompt() {
    // println!("Running prompt...");
    // let mut buffer = String::new();
    // let stdin = std::io::stdin();
    // let mut interpreter = Interpreter::new();
    //
    // loop {
    //     if let Ok(count) = stdin.read_line(&mut buffer) {
    //         if count == 0 {
    //             break;
    //         }
    //
    //         if let Ok(tokens) = Lexer::scan_tokens(&buffer) {
    //             if let Ok(statements) = Parser::parse(tokens) {
    //                 interpreter.interpret(&statements);
    //             }
    //         }
    //
    //         buffer.clear();
    //     }
    // }
}
