use std::collections::HashMap;

use crate::expr::Expr;
use crate::stmt::Stmt;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub struct ExprId {
    idx: usize,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub struct StmtId {
    idx: usize,
}

/// An representation of an executable program in the form of an AST.
/// Nodes are stored in flat arrays and reference children via ID.
pub struct Program {
    pub main: Vec<StmtId>,
    pub statements: Vec<Stmt>,
    pub expressions: Vec<Expr>,
    pub resolves: HashMap<ExprId, usize>,
}

impl Program {
    pub fn push_stmt(&mut self, stmt: Stmt) -> StmtId {
        let idx = self.statements.len();
        self.statements.push(stmt);

        StmtId {
            idx
        }
    }

    pub fn push_expr(&mut self, expr: Expr) -> ExprId {
        let idx = self.expressions.len();
        self.expressions.push(expr);

        ExprId {
            idx
        }
    }

    pub fn remove_expr_and_children(&mut self, id: ExprId) {
        self.expressions.truncate(id.idx);
    }

    pub fn get_stmt(&self, id: &StmtId) -> &Stmt {
        &self.statements[id.idx]
    }

    pub fn get_expr(&self, id: &ExprId) -> &Expr {
        &self.expressions[id.idx]
    }
}