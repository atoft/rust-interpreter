use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

use crate::{interpreter::Value, lexer::Token};

pub struct Environment {
    enclosing: Option<Rc<RefCell<Environment>>>,
    values: HashMap<String, Value>,
}

impl Environment {
    pub fn new() -> Self {
        Self {
            enclosing: None,
            values: HashMap::new(),
        }
    }

    pub fn set_enclosing(env: Rc<RefCell<Self>>, parent: Rc<RefCell<Self>>) {
        env.borrow_mut().enclosing = Some(Rc::clone(&parent));
    }

    pub fn extract(env: &mut Rc<RefCell<Self>>) {
        if let None = env.borrow_mut().enclosing {
            panic!("Tried to extract the top level environment!.");
        }

        let enclosing = std::mem::take(&mut env.borrow_mut().enclosing);
        *env = enclosing.unwrap();
    }
    
    pub fn get(&self, name: &String) -> Option<Value> {
        return if let Some(val) = self.values.get(name) {
            Some(val.clone())
        } else if let Some(env) = &self.enclosing {
            env.borrow().get(name)
        } else {
            None
        }
    }

    pub fn get_at(&self, distance: usize, name: &String) -> Option<Value> {
        if distance == 0 {
            self.get(&name)
        } else {
            Environment::ancestor(Rc::clone(&self.enclosing.as_ref().unwrap()), distance - 1).borrow().get(&name)
        }
    }

    pub fn assign(&mut self, name: &Token, value: Value) -> Option<()> {
        if let Some(val) = self.values.get_mut(&name.lexeme) {
            *val = value;
            Some(())
        } else if let Some(env) = &mut self.enclosing {
            env.borrow_mut().assign(name, value)
        } else {
            None
        }
    }

    pub fn assign_at(&mut self, distance: usize, name: &Token, value: Value) -> Option<()> {
        // @Lang What a mess. The Option wrangling and *val assignment couldn't be moved outside
        // the outermost if because it would mean that ancestor would go out of scope too soon.
        // Also apparently `result` has to be a local rather than returned directly, because...
        // ```the borrow might be used here, when that temporary is dropped and runs the destructor for type `RefMut<'_, Environment>````
        //...?
        if distance == 0 {
            if let Some(val) = self.values.get_mut(&name.lexeme) {
                *val = value;
                Some(())
            } else {
                None
            }
        } else {
            let ancestor = Environment::ancestor(Rc::clone(&self.enclosing.as_ref().unwrap()), distance - 1);
            let result = if let Some(val) = ancestor.borrow_mut().values.get_mut(&name.lexeme){
                *val = value;
                Some(())
            } else {
                None
            };
            result
        }
    }

    fn ancestor(env: Rc<RefCell<Self>>, distance: usize) -> Rc<RefCell<Environment>> {
        let mut current_ancestor = env;
        for _i in 0..distance {
            let next = Rc::clone(&current_ancestor.borrow_mut().enclosing.as_ref().unwrap());
            current_ancestor = next;
        }

        current_ancestor
    }

    pub fn define(&mut self, name: String, value: Value) {
        self.values.insert(name, value);
    }
}
