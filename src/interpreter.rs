use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

use crate::{environment::Environment, error::*, expr::*, lexer::{Token, TokenType}, stmt::*};
use crate::lox_class::LoxClass;
use crate::lox_function::LoxFunction;
use crate::lox_instance::LoxInstance;
use crate::lox_native::{LoxNative};
use crate::program::{ExprId, Program, StmtId};
use std::ops::Deref;
use std::time::SystemTime;
use std::io;

pub enum Value {
    Number(f32),
    String(String),
    Bool(bool),
    Callable(Rc<Callable>),
    Instance(Rc<RefCell<LoxInstance>>),
    Nil,
}

pub enum Callable {
    Function(LoxFunction),
    Class(LoxClass),
    Native(LoxNative),
}

impl Callable {
    pub fn call(&self, interpreter: &mut Interpreter, program: &Program, arguments: Vec<Value>) -> MaybeValue {
        // @Lang no way to collapse this in Rust?
        match self {
            Callable::Function(x) => x.call(interpreter, program, arguments),
            Callable::Class(x) => x.call(interpreter, program, arguments),
            Callable::Native(x) => x.call(interpreter, program, arguments),
        }
    }

    pub fn arity(&self) -> usize {
        // @Lang no way to collapse this in Rust?
        match self {
            Callable::Function(x) => x.arity(),
            Callable::Class(x) => x.arity(),
            Callable::Native(x) => x.arity(),
        }
    }

    pub fn to_string(&self) -> String {
        // @Lang no way to collapse this in Rust?
        match self {
            Callable::Function(x) => x.to_string(),
            Callable::Class(x) => x.to_string(),
            Callable::Native(x) => x.to_string(),
        }
    }
}

impl Clone for Value {
    fn clone(&self) -> Self {
        match self {
            Value::Callable(callable) => Value::Callable(Rc::clone(callable)),
            Value::Instance(instance) => Value::Instance(Rc::clone(instance)),

            Value::Number(x) => Value::Number(*x),
            Value::String(x) => Value::String(x.clone()),
            Value::Bool(x) => Value::Bool(*x),
            Value::Nil => Value::Nil,
        }
    }
}

pub struct RuntimeError {
    pub message: String,
    pub token: Token,
}

pub enum Execution {
    Continue,
    Break(Token),
    Return(Value),
}

pub type MaybeValue = Result<Value, RuntimeError>;
pub type MaybeExec = Result<Execution, RuntimeError>;

pub struct Interpreter {
    pub globals: Rc<RefCell<Environment>>,
    environment: Rc<RefCell<Environment>>,
    pub had_runtime_error: bool,
}

impl Interpreter {
    pub fn interpret(program: Program) -> bool {
        let globals = Rc::new(RefCell::new(Environment::new()));

        let mut interpreter = Interpreter {
            globals: Rc::clone(&globals),
            environment: globals,
            had_runtime_error: false,
        };

        interpreter.globals.borrow_mut().define(String::from("clock"), Value::Callable(Rc::new(Callable::Native(LoxNative::new(
            |_args| {
                match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
                    Ok(n) => Ok(Value::Number(n.as_secs_f32())),
                    Err(_) => panic!("SystemTime before UNIX EPOCH!"),
                }
            },
        )))));

        interpreter.globals.borrow_mut().define(String::from("readline"), Value::Callable(Rc::new(Callable::Native(LoxNative::new(
            |_args| {
                let mut buffer = String::new();
                let stdin = io::stdin();
                if let Ok(_) = stdin.read_line(&mut buffer) {
                    Ok(Value::String(str::replace(&*buffer, "\n", "")))
                } else {
                    // Can't return a RuntimeError here because we don't have a Token.
                    panic!("readline runtime error!")
                }
            },
        )))));

        let main = program.main.clone();

        for statement in &main {
            match interpreter.execute(&program, statement) {
                Ok(execution) => {
                    match execution {
                        Execution::Continue => continue,
                        Execution::Break(_) => {
                            panic!("Tried to break not inside loop! Should be caught in Resolver.");
                        },
                        Execution::Return(_) => {
                            panic!("Tried to return from top level statement! Should be caught in Resolver.");
                        }
                    }
                },
                Err(err) => {
                    error_for_token(&err.token, err.message);
                    interpreter.had_runtime_error = true;
                    break;
                },
            }
        }

        !interpreter.had_runtime_error
    }

    fn execute(&mut self, program: &Program, statement: &StmtId) -> MaybeExec {
        self.visit_stmt(program, statement)
    }

    pub fn execute_block(&mut self, program: &Program, statements: &Vec<StmtId>, environment: Rc<RefCell<Environment>>) -> MaybeExec {
        let previous_env = Rc::clone(&self.environment);
        self.environment = environment;

        let mut result = Ok(Execution::Continue);
        for stmt in statements {
            match self.execute(program, stmt) {
                Ok(execution) => match execution {
                    Execution::Continue => continue,
                    Execution::Break(_) =>  {
                        result = Ok(execution);
                        break;
                    },
                    Execution::Return(_) => {
                        result = Ok(execution);
                        break;
                    }
                },
                Err(e) => {
                    result = Err(e);
                    break;
                },
            }
        }

        self.environment = previous_env;
        
        result
    }

    fn stringify(value: &Value) -> String {
        match value {
            Value::Number(x) => format!("{}", x),
            Value::String(x) => format!("{}", x),
            Value::Bool(x) => format!("{}", x),
            Value::Nil => format!("nil"),
            Value::Callable(x) => x.to_string(),
            Value::Instance(x) => x.borrow().to_string(),
        }
    }

    fn error(message: String, token: Token) -> MaybeValue {
        Err(RuntimeError {
            message,
            token,
        })
    }

    fn error_exec(message: String, token: Token) -> MaybeExec {
        Err(RuntimeError {
            message,
            token,
        })
    }
    
    fn evaluate(&mut self, program: &Program, id: &ExprId) -> MaybeValue {
        let expr = program.get_expr(id);
        self.visit_expr(program, id, expr)
    }
    
    fn is_truthy(value: &Value) -> bool {
        match *value {
            Value::Nil => false,
            Value::Bool(b) => b,
            _ => true,
        }
    }

    fn is_equal(left: &Value, right: &Value) -> bool {
        match (&left, &right) {
            (Value::Nil, Value::Nil) => {
                return true;
            },
            (Value::Nil, _) => {
                return false;
            },
            (Value::Number(lhs), Value::Number(rhs)) => {
                return lhs == rhs;
            },
            (Value::Bool(lhs), Value::Bool(rhs)) => {
                return lhs == rhs;
            },
            (Value::String(ref lhs), Value::String(ref rhs)) => {
                return lhs.eq(rhs);
            },
            _ => return false,
        }
    }

    fn error_not_numbers(token: &Token) -> MaybeValue {
        Interpreter::error(String::from("Operands must be numbers."), token.clone())
    }

    fn visit_stmt(&mut self, program: &Program, id: &StmtId) -> MaybeExec {
        let stmt = program.get_stmt(id);
        match stmt {
            Stmt::Print(e) => self.visit_print(program, &e),
            Stmt::Function(e) => self.visit_function_stmt(program, id, &e),
            Stmt::If(e) => self.visit_if_stmt(program, &e),
            Stmt::Expr(e) => self.visit_expression_stmt(program, &e),
            Stmt::Var(e) => self.visit_var_stmt(program, &e),
            Stmt::Return(e) => self.visit_return_stmt(program, &e),
            Stmt::While(e) => self.visit_while_stmt(program, &e),
            Stmt::Break(e) => self.visit_break_stmt(program, &e),
            Stmt::Block(e) => self.visit_block_stmt(program, &e),
            Stmt::Class(e) => self.visit_class_stmt(program, &e),
        }
    }

    fn visit_expr(&mut self, program: &Program, id: &ExprId, expr: &Expr) -> MaybeValue {
        match expr {
            Expr::Assign(e) => self.visit_assign(program, id, &e),
            Expr::Binary(e) => self.visit_binary(program, &e),
            Expr::Call(e) => self.visit_call(program, &e),
            Expr::Get(e) => self.visit_get(program, id, &e),
            Expr::Grouping(e) => self.visit_grouping(program, &e),
            Expr::Literal(e) => self.visit_literal(program, &e),
            Expr::Logical(e) => self.visit_logical(program, &e),
            Expr::Set(e) => self.visit_set(program, id, &e),
            Expr::Super(e) => self.visit_super(program, id, &e),
            Expr::This(e) => self.visit_this(program, id, &e),
            Expr::Unary(e) => self.visit_unary(program, &e),
            Expr::Var(e) => self.visit_var(program, id, &e),
        }
    }

    fn visit_assign(&mut self, program: &Program, id: &ExprId, expr: &AssignExpr) -> MaybeValue {
        let value = self.evaluate(program, &expr.value)?;
        if let Some(distance) = program.resolves.get(id) {
            self.environment.borrow_mut().assign_at(*distance, &expr.name, value.clone())
        } else {
            self.globals.borrow_mut().assign(&expr.name, value.clone())
        };

        return Ok(value);
    }

    fn visit_binary(&mut self, program: &Program, expr: &BinaryExpr) -> MaybeValue {
        let left = self.evaluate(program, &expr.left)?;
        let right = self.evaluate(program, &expr.right)?;

        match &expr.operator.token_type {
            TokenType::Greater => {
                if let (Value::Number(lhs), Value::Number(rhs)) = (left, right) {
                    return Ok(Value::Bool(lhs > rhs));
                } else {
                    return Interpreter::error_not_numbers(&expr.operator);
                }
            },
            TokenType::GreaterEqual => {
                if let (Value::Number(lhs), Value::Number(rhs)) = (left, right) {
                    return Ok(Value::Bool(lhs >= rhs));
                } else {
                    return Interpreter::error_not_numbers(&expr.operator);
                }
            },
            TokenType::Less => {
                if let (Value::Number(lhs), Value::Number(rhs)) = (left, right) {
                    return Ok(Value::Bool(lhs < rhs));
                } else {
                    return Interpreter::error_not_numbers(&expr.operator);
                }
            },
            TokenType::LessEqual => {
                if let (Value::Number(lhs), Value::Number(rhs)) = (left, right) {
                    return Ok(Value::Bool(lhs <= rhs));
                } else {
                    return Interpreter::error_not_numbers(&expr.operator);
                }
            },

            TokenType::EqualEqual => {
                return Ok(Value::Bool(Interpreter::is_equal(&left, &right)));
            },
            TokenType::BangEqual => {
                return Ok(Value::Bool(!Interpreter::is_equal(&left, &right)));
            }

            TokenType::Minus => {
                if let (Value::Number(lhs), Value::Number(rhs)) = (left, right) {
                    return Ok(Value::Number(lhs - rhs));
                } else {
                    return Interpreter::error_not_numbers(&expr.operator);
                }
            },
            TokenType::Slash => {
                if let (Value::Number(lhs), Value::Number(rhs)) = (left, right) {
                    return Ok(Value::Number(lhs / rhs));
                } else {
                    return Interpreter::error_not_numbers(&expr.operator);
                }
            },
            TokenType::Star => {
                if let (Value::Number(lhs), Value::Number(rhs)) = (left, right) {
                    return Ok(Value::Number(lhs * rhs));
                } else {
                    return Interpreter::error_not_numbers(&expr.operator);
                }
            },
            TokenType::Plus => {
                if let (Value::Number(lhs), Value::Number(rhs)) = (&left, &right) {
                    return Ok(Value::Number(lhs + rhs));
                } else if let (Value::String(lhs), Value::String(rhs)) = (left, right) {
                    return Ok(Value::String(String::from(format!("{}{}", lhs, rhs))));
                } else {
                    return Interpreter::error(String::from("Values must be two numbers or two strings."), expr.operator.clone());
                }
            },
            other => panic!("Invalid binary token type {:?}", other),
        }
    }

    fn visit_call(&mut self, program: &Program, expr: &CallExpr) -> MaybeValue {
        let callee = self.evaluate(program, &expr.callee)?;

        let mut arguments = Vec::new();

        for arg in &expr.arguments {
            arguments.push(self.evaluate(program, arg)?);
        }

        match callee {
            Value::Callable(f) => {
                if f.arity() != arguments.len() {
                    return Err(RuntimeError {
                        message: format!("Mismatched arity (expect {} arguments, found {}).", f.arity(), arguments.len()),
                        token: expr.paren.clone(),
                    })
                }
                f.call(self, program, arguments)
            },
            _ => Err(RuntimeError {
                message: String::from("Can only call functions and classes."),
                token: expr.paren.clone(),
            })
        }
    }

    fn visit_grouping(&mut self, program: &Program, expr: &GroupingExpr) -> MaybeValue {
        self.evaluate(program, &expr.expr)
    }

    fn visit_literal(&mut self, _program: &Program, expr: &LiteralExpr) -> MaybeValue {
        Ok(match *expr {
            LiteralExpr::Number(x) => Value::Number(x),
            LiteralExpr::String(ref x) => Value::String(x.clone()),
            LiteralExpr::Bool(x) => Value::Bool(x),
            LiteralExpr::Nil => Value::Nil,
        })
    }

    fn visit_logical(&mut self, program: &Program, expr: &LogicalExpr) -> MaybeValue {
        let left = self.evaluate(program, &expr.left)?;

        match expr.operator.token_type {
            TokenType::And => {
                if !Interpreter::is_truthy(&left) {
                    return Ok(left)
                }
                self.evaluate(program, &expr.right)
            }
            TokenType::Or => {
                if Interpreter::is_truthy(&left) {
                    return Ok(left)
                }
                self.evaluate(program, &expr.right)
            }
            _ => panic!("Unexpected token type for logical operator")
        }
    }

    fn visit_unary(&mut self, program: &Program, expr: &UnaryExpr) -> MaybeValue {
        let right = self.evaluate(program, &expr.right)?;

        // TODO It would be better to have a type for only the allowed operands, so it's not possible to
        // have the unreachable state here.
        match expr.operator.token_type {
            TokenType::Bang => {
                return Ok(Value::Bool(!Interpreter::is_truthy(&right)));
            }
            TokenType::Minus => {
                if let Value::Number(num) = right {
                    return Ok(Value::Number(-num));
                } else {
                    return Interpreter::error(String::from("Operand must be a number."), expr.operator.clone());
                }
            },
            _ => panic!(),
        }
    }

    fn visit_var(&mut self, program: &Program, id: &ExprId, expr: &VarExpr) -> MaybeValue {
        self.lookup_variable(program, &expr.name, id)
    }

    fn visit_get(&mut self, program: &Program, _id: &ExprId, expr: &GetExpr) -> MaybeValue {
        let value = self.evaluate(program, &expr.object)?;

        if let Value::Instance(instance) = value {
            if let Some(val) = LoxInstance::get(instance, program, &expr.name) {
                Ok(val)
            } else {
                Interpreter::error(format!("Undefined property '{}'.", &expr.name.lexeme), expr.name.clone())
            }
        } else {
            Interpreter::error(String::from("Only instances have properties."), expr.name.clone())
        }
    }

    fn visit_set(&mut self, program: &Program, _id: &ExprId, expr: &SetExpr) -> MaybeValue {
        let object = self.evaluate(program, &expr.object)?;

        if let Value::Instance(instance) = object {
            let value = self.evaluate(program, &expr.value)?;
            instance.borrow_mut().set(expr.name.clone(), value.clone());
            Ok(value)
        } else {
            Interpreter::error(String::from("Only instances have fields."), expr.name.clone())
        }
    }

    fn visit_super(&mut self, program: &Program, id: &ExprId, expr: &SuperExpr) -> MaybeValue {
        let distance = program.resolves.get(id).expect("Unresolved reference to super!");

        let superclass = self.environment.borrow_mut().get_at(*distance, &String::from("super")).expect("Super not in environment!");

        // The environment where "this" is bound is always right inside the environment where we store "super".
        let this_distance = *distance - 1;
        let this_object = self.environment.borrow_mut().get_at(this_distance, &String::from("this")).expect("This not in environment!");

        let class = if let Value::Callable(callable) = &superclass {
            if let Callable::Class(class) = callable.deref() {
                class
            } else {
                panic!("Superclass not a class!")
            }
        } else {
            panic!("Superclass not a callable!")
        };

        let method = class.find_method(&expr.method.lexeme);

        if let Some(function) = method {
            let instance = if let Value::Instance(inst) = this_object {
                inst
            } else {
                panic!("This not an instance!")
            };

            Ok(Value::Callable(Rc::new(Callable::Function(function.bind(program, instance)))))
        } else {
            Interpreter::error(format!("Undefined property '{}'.", &expr.method.lexeme), expr.method.clone())
        }
    }

    fn visit_this(&mut self, program: &Program, id: &ExprId, expr: &ThisExpr) -> MaybeValue {
        self.lookup_variable(program, &expr.keyword, id)
    }

    fn lookup_variable(&mut self, program: &Program, name: &Token, id: &ExprId) -> MaybeValue {
        let option = if let Some(distance) = program.resolves.get(id) {
            self.environment.borrow().get_at(*distance, &name.lexeme)
        } else {
            self.globals.borrow().get(&name.lexeme)
        };

        if let Some(value) = option {
            Ok(value)
        } else {
            Interpreter::error(format!("Undefined variable '{}'.", name.lexeme), name.clone())
        }
    }

    fn visit_print(&mut self, program: &Program, stmt: &PrintStmt) -> MaybeExec {
        let val = self.evaluate(program, &stmt.expr)?;

        println!("{}", Interpreter::stringify(&val));

        Ok(Execution::Continue)
    }

    fn visit_function_stmt(&mut self, program: &Program, id: &StmtId, stmt: &FunctionStmt) -> MaybeExec {
        let callable = Rc::new(Callable::Function(LoxFunction::new(program, id.clone(), Rc::clone(&self.environment), false)));

        self.environment.borrow_mut().define(stmt.name.lexeme.clone(), Value::Callable(callable));

        Ok(Execution::Continue)
    }

    fn visit_if_stmt(&mut self, program: &Program, stmt: &IfStmt) -> MaybeExec {
        let condition = self.evaluate(program, &stmt.condition)?;

        let execution = if Interpreter::is_truthy(&condition) {
            self.execute(program, &stmt.then_branch)?
        } else if let Some(else_branch) = &stmt.else_branch {
            self.execute(program, &else_branch)?
        } else {
            Execution::Continue
        };

        Ok(execution)
    }

    fn visit_expression_stmt(&mut self, program: &Program, stmt: &ExpressionStmt) -> MaybeExec {
        self.evaluate(program, &stmt.expr)?;

        Ok(Execution::Continue)
    }

    fn visit_var_stmt(&mut self, program: &Program, stmt: &VarStmt) -> MaybeExec {
        let value = if let Some(init) = &stmt.initializer {
            self.evaluate(program, init)?
        } else {
            Value::Nil
        };

        self.environment.borrow_mut().define(stmt.name.lexeme.clone(), value);
        Ok(Execution::Continue)
    }

    fn visit_return_stmt(&mut self, program: &Program, stmt: &ReturnStmt) -> MaybeExec {
        let value = if let Some(expr) = &stmt.value {
            self.evaluate(program, &expr)?
        } else {
            Value::Nil
        };

        Ok(Execution::Return(value))
    }

    fn visit_while_stmt(&mut self, program: &Program, stmt: &WhileStmt) -> MaybeExec {
        while Interpreter::is_truthy(&self.evaluate(program, &stmt.condition)?) {
            let execution = self.execute(program, &stmt.body)?;

            if let Execution::Break(_) = execution {
                break;
            }
        }
        Ok(Execution::Continue)
    }

    fn visit_break_stmt(&mut self, _program: &Program, stmt: &BreakStmt) -> MaybeExec {
        Ok(Execution::Break(stmt.location.clone()))
    }

    fn visit_block_stmt(&mut self, program: &Program, stmt: &BlockStmt) -> MaybeExec {
        let environment = Rc::new(RefCell::new(Environment::new()));
        Environment::set_enclosing(Rc::clone(&environment), Rc::clone(&self.environment));
        self.execute_block(program, &stmt.statements, environment)
    }

    fn visit_class_stmt(&mut self, program: &Program, stmt: &ClassStmt) -> MaybeExec {
        let superclass = if let Some(superclass_expr) = stmt.superclass {
            let superclass_value = self.evaluate(program, &superclass_expr)?;

            if let Value::Callable(callable) = superclass_value {
                Some(callable)
            } else {
                if let Expr::Var(superclass_var) = program.get_expr(&superclass_expr) {
                    return Interpreter::error_exec(String::from("Superclass must be a class."), superclass_var.name.clone());
                } else {
                    panic!("Superclass not a VarExpr!")
                }
            }
        } else {
            None
        };

        self.environment.borrow_mut().define(stmt.name.lexeme.clone(), Value::Nil);

        let previous_environment = Rc::clone(&self.environment);

        let had_superclass = if let Some(superc) = &superclass {
            let inner_environment = Rc::new(RefCell::new(Environment::new()));
            Environment::set_enclosing(Rc::clone(&inner_environment), Rc::clone(&self.environment));
            self.environment = inner_environment;
            self.environment.borrow_mut().define(String::from("super"), Value::Callable(Rc::clone(superc)));
            true
        } else {
            false
        };

        let mut methods = HashMap::new();
        for method in &stmt.methods {
            let method_stmt = program.get_stmt(method);

            if let Stmt::Function(function_stmt) = method_stmt {
                let is_initializer = function_stmt.name.lexeme == "init";
                let callable = Rc::new(LoxFunction::new(program, method.clone(), Rc::clone(&self.environment), is_initializer));
                methods.insert(function_stmt.name.lexeme.clone(), callable);
            } else {
                panic!("Tried to add a method that was not a FunctionStmt!");
            }
        }

        let class = LoxClass::new(stmt.name.lexeme.clone(), superclass, methods);

        if had_superclass {
            self.environment = previous_environment;
        }

        self.environment.borrow_mut().assign(&stmt.name, Value::Callable(Rc::new(Callable::Class(class))));
        Ok(Execution::Continue)
    }
}
