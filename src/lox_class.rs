use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

use crate::interpreter::{Callable, Interpreter, MaybeValue, Value};
use crate::lox_function::LoxFunction;
use crate::lox_instance::LoxInstance;
use crate::program::Program;

#[derive(Clone)]
pub struct LoxClass {
    // Double indirection is not great. call() needs to create an instance with an Rc of its class
    // but the dynamic dispatch gives us &self and not Rc<Self>.
    pub imp: Rc<ClassImpl>
}

pub struct ClassImpl {
    pub name: String,
    pub superclass: Option<Rc<Callable>>,   // NOTE: Should be Rc<LoxClass> - maybe callables should be an enum instead of a trait object?
    pub methods: HashMap<String, Rc<LoxFunction>>,
}

impl LoxClass {
    pub fn new(name: String, superclass: Option<Rc<Callable>>, methods: HashMap<String, Rc<LoxFunction>>) -> LoxClass{
        LoxClass {
            imp: Rc::new(ClassImpl {
                name,
                superclass,
                methods
            })
        }
    }

    pub fn find_method(&self, name: &String) -> Option<Rc<LoxFunction>> {
        self.imp.find_method(name)
    }

    pub fn call(&self, interpreter: &mut Interpreter, program: &Program, arguments: Vec<Value>) -> MaybeValue {
        let instance = LoxInstance::new(Rc::clone(&self.imp));

        let rc_instance = Rc::new(RefCell::new(instance));

        if let Some(init) = self.imp.find_method(&String::from("init")) {
            init.bind(program, Rc::clone(&rc_instance)).call(interpreter, program, arguments)?;
        }

        Ok(Value::Instance(rc_instance))
    }

    pub fn arity(&self) -> usize {
        if let Some(init) = self.imp.find_method(&String::from("init")) {
            return init.arity();
        }

        0
    }

    pub fn to_string(&self) -> String {
        self.imp.name.clone()
    }
}

impl ClassImpl {
    pub fn find_method(&self, name: &String) -> Option<Rc<LoxFunction>> {
        if let Some(method) = self.methods.get(name) {
            Some(method.clone())
        } else {
            None
        }
    }
}