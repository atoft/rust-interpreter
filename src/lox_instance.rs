use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

use crate::interpreter::{Value, Callable};
use crate::lexer::Token;
use crate::lox_class::ClassImpl;
use crate::program::Program;

pub struct LoxInstance {
    pub class: Rc<ClassImpl>,
    fields: HashMap<String, Value>,
}

impl LoxInstance {
    pub fn new(class: Rc<ClassImpl>) -> LoxInstance {
        LoxInstance {
            class,
            fields: HashMap::new(),
        }
    }

    pub fn to_string(&self) -> String {
        format!("{} instance", self.class.name)
    }

    pub fn get(rc_self: Rc<RefCell<LoxInstance>>, program: &Program, name: &Token) -> Option<Value> {
        if let Some(val) = rc_self.borrow().fields.get(&name.lexeme) {
            Some(val.clone())
        } else if let Some(val) = rc_self.borrow().class.find_method(&name.lexeme) {
            Some(Value::Callable(Rc::new(Callable::Function(val.bind(program, Rc::clone(&rc_self))))))
        } else {
            None
        }
    }

    pub fn set(&mut self, name: Token, value: Value) {
        self.fields.insert(name.lexeme, value);
    }
}