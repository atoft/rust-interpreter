use crate::error::error;

#[derive(Clone, Debug, PartialEq)]
pub enum TokenType {
    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    Comma,
    Dot,
    Minus,
    Plus,
    Semicolon,
    Slash,
    Star,

    Bang,
    BangEqual,
    Equal,
    EqualEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual,

    Identifier,
    String,
    Number,

    And,
    Break,
    Class,
    Else,
    False,
    Fun,
    For,
    If,
    Nil,
    Or,
    Print,
    Return,
    Super,
    This,
    True,
    Var,
    While,

    EOF,
}

#[derive(Clone, Debug)]
pub enum Literal {
    Identifier(String),
    String(String),
    Number(f32),
}

#[derive(Clone, Debug)]
pub struct Token {
    pub token_type: TokenType,
    pub literal: Option<Literal>,
    pub lexeme: String,
    pub line: usize,
}

pub struct Lexer {
    chars: Vec<char>,
    tokens: Vec<Token>,
    start: usize,
    current: usize,
    line: usize,
}

impl Lexer {
    pub fn scan_tokens(source: &String) -> Result<Vec<Token>, String> {
        let tokens: Vec<Token> = Vec::new();
        let chars: Vec<char> = source.chars().collect();

        let mut lexer = Lexer {
            chars,
            tokens,
            start: 0,
            current: 0,
            line: 1,
        };

        while !lexer.is_at_end() {
            lexer.scan_token()?;
        }

        lexer.tokens.push(Token {
            token_type: TokenType::EOF,
            literal: None,
            lexeme: String::new(),
            line: lexer.line,
        });
        Ok(lexer.tokens)
    }

    fn scan_token(&mut self) -> Result<(), String> {
        // Starting a new token.
        self.start = self.current;

        let character = self.advance();

        let token_type = match character {
            '(' => TokenType::LeftParen,
            ')' => TokenType::RightParen,
            '{' => TokenType::LeftBrace,
            '}' => TokenType::RightBrace,
            ',' => TokenType::Comma,
            '.' => TokenType::Dot,
            '-' => TokenType::Minus,
            '+' => TokenType::Plus,
            ';' => TokenType::Semicolon,
            '*' => TokenType::Star,

            '!' => {
                if self.advance_if('=') {
                    TokenType::BangEqual
                } else {
                    TokenType::Bang
                }
            }
            '=' => {
                if self.advance_if('=') {
                    TokenType::EqualEqual
                } else {
                    TokenType::Equal
                }
            }
            '<' => {
                if self.advance_if('=') {
                    TokenType::LessEqual
                } else {
                    TokenType::Less
                }
            }
            '>' => {
                if self.advance_if('=') {
                    TokenType::GreaterEqual
                } else {
                    TokenType::Greater
                }
            }

            '/' => {
                if self.advance_if('/') {
                    // It's a comment, continue to end of line.
                    while self.peek() != '\n' && !self.is_at_end() {
                        self.advance();
                    }
                    return Ok(());
                } else {
                    TokenType::Slash
                }
            }

            ' ' | '\r' | '\t' => {
                // Skip whitespace.
                return Ok(());
            }
            '\n' => {
                self.line += 1;
                return Ok(());
            }

            '"' => {
                return self.lex_string();
            }

            digit if Lexer::is_digit(digit) => {
                self.lex_number();
                return Ok(());
            }
            alpha if Lexer::is_alphabetic(alpha) => {
                self.lex_identifier();
                return Ok(());
            }
            _ => {
                return Err(error(
                    self.line,
                    format!("Unrecognised character '{}'", character),
                ));
            }
        };

        self.add_token(token_type, None);
        Ok(())
    }

    fn lex_string(&mut self) -> Result<(), String> {
        while self.peek() != '"' && !self.is_at_end() {
            if self.peek() == '\n' {
                self.line += 1;
            }

            self.advance();
        }

        if self.is_at_end() {
            return Err(error(self.line, "Unterminated string.".to_string()));
        }

        // Skip the closing quote.
        self.advance();

        let literal = self.chars[self.start + 1..self.current - 1]
            .into_iter()
            .collect();
        self.add_token(TokenType::String, Some(Literal::String(literal)));
        Ok(())
    }

    fn lex_number(&mut self) {
        while Lexer::is_digit(self.peek()) {
            self.advance();
        }

        if self.peek() == '.' && Lexer::is_digit(self.peek_next()) {
            self.advance();

            while Lexer::is_digit(self.peek()) {
                self.advance();
            }
        }

        let literal = self.chars[self.start..self.current]
            .into_iter()
            .collect::<String>()
            .parse::<f32>()
            .unwrap();
        self.add_token(TokenType::Number, Some(Literal::Number(literal)));
    }

    fn lex_identifier(&mut self) {
        while self.peek().is_alphanumeric() {
            self.advance();
        }

        let name: String = self.chars[self.start..self.current].into_iter().collect();

        let keyword_type = match name.as_str() {
            "and" => Some(TokenType::And),
            "break" => Some(TokenType::Break),
            "class" => Some(TokenType::Class),
            "else" => Some(TokenType::Else),
            "false" => Some(TokenType::False),
            "fun" => Some(TokenType::Fun),
            "for" => Some(TokenType::For),
            "if" => Some(TokenType::If),
            "nil" => Some(TokenType::Nil),
            "or" => Some(TokenType::Or),
            "print" => Some(TokenType::Print),
            "return" => Some(TokenType::Return),
            "super" => Some(TokenType::Super),
            "this" => Some(TokenType::This),
            "true" => Some(TokenType::True),
            "var" => Some(TokenType::Var),
            "while" => Some(TokenType::While),
            _ => None,
        };

        if let Some(token_type) = keyword_type {
            self.add_token(token_type, None);
        } else {
            self.add_token(TokenType::Identifier, Some(Literal::Identifier(name)));
        }
    }

    fn is_digit(character: char) -> bool {
        character >= '0' && character <= '9'
    }

    fn is_alphabetic(character: char) -> bool {
        character.is_alphabetic()
    }

    fn is_at_end(&self) -> bool {
        self.current >= self.chars.len()
    }

    fn advance(&mut self) -> char {
        let result = self.chars[self.current];
        self.current += 1;
        result
    }

    fn advance_if(&mut self, query: char) -> bool {
        if self.is_at_end() {
            return false;
        }

        if self.chars[self.current] != query {
            return false;
        }

        self.current += 1;
        true
    }

    fn peek(&self) -> char {
        if self.is_at_end() {
            return '\0';
        }

        return self.chars[self.current];
    }

    fn peek_next(&self) -> char {
        if self.current + 1 >= self.chars.len() {
            return '\0';
        }

        return self.chars[self.current + 1];
    }

    fn add_token(&mut self, token_type: TokenType, literal: Option<Literal>) {
        let lexeme: String = self.chars[self.start..self.current].into_iter().collect();
        self.tokens.push(Token {
            token_type,
            literal,
            lexeme,
            line: self.line,
        });
    }
}
