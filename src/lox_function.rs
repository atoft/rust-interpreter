use std::cell::RefCell;
use std::rc::Rc;

use crate::environment::Environment;
use crate::interpreter::*;
use crate::lox_instance::LoxInstance;
use crate::program::{Program, StmtId};
use crate::stmt::Stmt;

#[derive(Clone)]
pub struct LoxFunction {
    pub declaration: StmtId,
    pub closure: Rc<RefCell<Environment>>,
    arity: usize,
    name: String,
    is_initializer: bool,
}

impl LoxFunction {
    pub fn new(program: &Program, declaration: StmtId, closure: Rc<RefCell<Environment>>, is_initializer: bool) -> LoxFunction {
        let function_stmt = if let Stmt::Function(s) = program.get_stmt(&declaration) {
            s
        } else {
            panic!("Function whose declaration is not a FunctionStmt!");
        };

        LoxFunction {
            declaration,
            closure,
            arity: function_stmt.params.len(),
            name: function_stmt.name.lexeme.clone(),
            is_initializer
        }
    }

    pub fn bind(&self, program: &Program, instance: Rc<RefCell<LoxInstance>>) -> LoxFunction {
        let environment = Rc::new(RefCell::new(Environment::new()));
        Environment::set_enclosing(Rc::clone(&environment), Rc::clone(&self.closure));
        environment.borrow_mut().define(String::from("this"), Value::Instance(instance));

        LoxFunction::new(program, self.declaration, environment, self.is_initializer)
    }

    pub fn call(&self, interpreter: &mut Interpreter, program: &Program, arguments: Vec<Value>) -> MaybeValue {
        let environment = Rc::new(RefCell::new(Environment::new()));
        Environment::set_enclosing(Rc::clone(&environment), Rc::clone(&self.closure));
        let function_stmt = if let Stmt::Function(s) = program.get_stmt(&self.declaration) {
            s
        } else {
            panic!("Function whose declaration is not a FunctionStmt!");
        };

        for (index, param) in function_stmt.params.iter().enumerate() {
            environment.borrow_mut().define(param.lexeme.clone(), arguments[index].clone());
        }

        let exec = interpreter.execute_block(program, &function_stmt.body, environment)?;

        let return_value = if let Execution::Return(value) = exec {
            if self.is_initializer {
                self.closure.borrow_mut().get_at(0, &String::from("this")).expect("No this when calling an initializer!")
            } else {
                value
            }
        } else {
            Value::Nil
        };

        Ok(return_value)
    }

    pub fn arity(&self) -> usize {
        self.arity
    }

    pub fn to_string(&self) -> String {
        self.name.clone()
    }
}