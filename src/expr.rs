use crate::lexer::Token;
use crate::program::*;

#[derive(Debug, Clone)]
pub struct AssignExpr {
    pub name: Token,
    pub value: ExprId,
}

#[derive(Debug, Clone)]
pub struct BinaryExpr {
    pub left: ExprId,
    pub operator: Token,
    pub right: ExprId,
}

#[derive(Debug, Clone)]
pub struct CallExpr {
    pub callee: ExprId,
    pub paren: Token,
    pub arguments: Vec<ExprId>,
}

#[derive(Debug, Clone)]
pub struct GetExpr {
    pub object: ExprId,
    pub name: Token,
}

#[derive(Debug, Clone)]
pub struct SetExpr {
    pub object: ExprId,
    pub name: Token,
    pub value: ExprId,
}

#[derive(Debug, Clone)]
pub struct SuperExpr {
    pub keyword: Token,
    pub method: Token,
}


#[derive(Debug, Clone)]
pub struct ThisExpr {
    pub keyword: Token,
}

#[derive(Debug, Clone)]
pub struct GroupingExpr {
    pub expr: ExprId,
}

#[derive(Debug, Clone)]
pub enum LiteralExpr {
    Number(f32),
    String(String),
    Bool(bool),
    Nil,
}

#[derive(Debug, Clone)]
pub struct LogicalExpr {
    pub left: ExprId,
    pub operator: Token,
    pub right: ExprId,
}

#[derive(Debug, Clone)]
pub struct UnaryExpr {
    pub operator: Token,
    pub right: ExprId,
}

#[derive(Debug, Clone)]
pub struct VarExpr {
    pub name: Token,
}

#[derive(Debug, Clone)]
pub enum Expr {
    Assign(AssignExpr),
    Binary(BinaryExpr),
    Call(CallExpr),
    Get(GetExpr),
    Grouping(GroupingExpr),
    Literal(LiteralExpr),
    Logical(LogicalExpr),
    Set(SetExpr),
    Super(SuperExpr),
    This(ThisExpr),
    Unary(UnaryExpr),
    Var(VarExpr),
}
