use crate::lexer::{Token, TokenType};

pub fn error(line: usize, message: String) -> String {
    report(line, String::new(), message)
}

pub fn error_for_token(token: &Token, message: String) -> String {
    if token.token_type == TokenType::EOF {
        report(token.line, String::from(" at end "), message)
    } else {
        report(token.line, String::from(std::format!(" at '{}'", token.lexeme)), message)
    }
    
}

fn report(line: usize, place: String, message: String) -> String {
    let formatted = format!("[line {}] Error{}: {}", line, place, message);
    eprintln!("{}", formatted);
    formatted
}
